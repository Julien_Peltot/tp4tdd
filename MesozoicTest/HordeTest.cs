﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]

        public void TestAddDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual(0, horde.GetDinosaurs().Count);
            horde.Horde_add(louis);
            Assert.AreEqual(1,horde.GetDinosaurs().Count);
            Assert.AreEqual(louis, horde.GetDinosaurs()[0]);
            horde.Horde_add(nessie);
            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            Assert.AreEqual(nessie, horde.GetDinosaurs()[1]);
        }


        [TestMethod]
        public void TestRemoveDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.Horde_add(louis);
            horde.Horde_add(nessie);

             Assert.AreEqual(2, horde.GetDinosaurs().Count);
            horde.Horde_delete(louis);
              Assert.AreEqual(1,horde.GetDinosaurs().Count);
            horde.Horde_delete(nessie);
              Assert.AreEqual(0,horde.GetDinosaurs().Count);
        } 


     /*   [TestMethod]
        public void TestIntroduceAll()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.Horde_add(louis);
            horde.Horde_add(nessie);

            string expected_introduction = "Je suis Louis le Stegausaurus, j'ai 12 ans\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n";

            Assert.AreEqual(expected_introduction, horde.IntroduceAll());
        } */
    }
}




