﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());

            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.AreEqual("Nessie", nessie.getName());
            Assert.AreEqual("Diplodocus", nessie.getSpecie());
            Assert.AreEqual(11, nessie.getAge());
            
            Dinosaur petitpied = new Dinosaur("Petitpied", "Diplodocus", 10);

            Assert.AreEqual("Petitpied", petitpied.getName());
            Assert.AreEqual("Diplodocus", petitpied.getSpecie());
            Assert.AreEqual(10, petitpied.getAge());

            Dinosaur vera = new Dinosaur("Vera", "Triceraptor", 9);

            Assert.AreEqual("Vera", vera.getName());
            Assert.AreEqual("Triceraptor", vera.getSpecie());
            Assert.AreEqual(9, vera.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

      /*  [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans. ", louis.sayHello());
            Assert.AreEqual("Je suis Nessie le Diplodocus, j'ai 11 ans. ", nessie.sayHello());
        } */ 

        public void TestDinosaurTest()
        {

            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }
    }
}
