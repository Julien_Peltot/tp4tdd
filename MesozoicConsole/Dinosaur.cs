﻿using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        protected string name;
        protected static string specie;
        protected int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            Dinosaur.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            string test;

            test = string.Format("Je suis {0} le {1}, j'ai {2} ans. ", this.name, Dinosaur.specie, this.age);

            return test;
        }

        public string roar()
        {
            return "Grrr";
        }
        public int getAge()
        {
            return this.age;
        }

        public string getName()
        {
            return this.name;
        }

        public string getSpecie()
        {
            return Dinosaur.specie;
        }

        public void setName(string name)
        {
            this.name = name;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public void setSpecies(string specie)
        {
            Dinosaur.specie = specie;
        }

        public string hug( Dinosaur dinosaur)
        {
            string test2;

            test2 = string.Format("Je suis {0} et je fais un calin à {1}", this.name, dinosaur.getName());

            return test2;
        }
    }
}