﻿using System.Text;
using System.Collections.Generic;

namespace Mesozoic
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs;

        public Horde ()
        {
            this.dinosaurs = new List<Dinosaur>();
                      
        }

        public void Horde_delete(Dinosaur dinosaur)
        {
           this.dinosaurs.Remove(dinosaur);

        }

        public void Horde_add(Dinosaur dinosaur)
        {
            this.dinosaurs.Add(dinosaur);

        }

        public string IntroduceAll()
        {
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Dinosaur dinosaur in this.dinosaurs)
            {
                introduce_builder.AppendFormat("{0}\n", dinosaur.sayHello());
            }
            return introduce_builder.ToString().Trim();
        }

        public List<Dinosaur> GetDinosaurs()
        {
            return this.dinosaurs;
        }


        public void  setDinosaur(List<Dinosaur> dinosaurs)
        {
            this.dinosaurs = dinosaurs;
        }
           


    }
}
 
   