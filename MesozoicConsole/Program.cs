﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
namespace MesozoicConsole
{

    public class Program
    {

        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur petitpied = new Dinosaur("Petitpied", "Diplodocus", 10);
            Dinosaur vera = new Dinosaur("Vera", "Triceraptor", 9);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis); 
            dinosaurs.Add(nessie);
            dinosaurs.Add(petitpied);
            dinosaurs.Add(vera);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine("Présentation de nos dinosaures:");

                Console.WriteLine(louis.sayHello());
                Console.WriteLine(nessie.roar());

                Console.WriteLine(louis.hug(nessie));
                Console.WriteLine(nessie.sayHello());

                Console.WriteLine(louis.hug(louis));
                Console.WriteLine(nessie.hug(louis));

                Console.WriteLine("\nCréation d'un troupeau");

                Horde horde = new Horde();
                horde.Horde_add(louis);
                horde.Horde_add(nessie);

                Console.WriteLine(horde.IntroduceAll());

               
            }

         /*   Dinosaur henry = new Dinosaur("Henry", 11, "Diplodocus");
            Console.WriteLine(henry.sayHello());
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello()); */



            Console.ReadKey();

        }
    }
    

}